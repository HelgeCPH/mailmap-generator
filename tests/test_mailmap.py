import tempfile
import subprocess
from mailmap_generator.mailmap import create_mailmap, compute_mailmap, mailmap_df_to_str
import pytest
import pandas as pd


def test_mailmap_to_str():
    mailmap_rows = (
        ("John Doe <john@doe.org>", "John Doe <John@Doe.org>"),
        ("John Doe <john@doe.org>", "John Doe <John@doe.org>"),
        ("John Doe <john@doe.org>", "john doe <john@doe.org>"),
        ("Max Example <hi@test.com>", "Max Exa <max@test.co.uk>"),
    )
    mailmap_df = pd.DataFrame(mailmap_rows, columns=("author", "author_alias"))
    result = mailmap_df_to_str(mailmap_df)

    assert (
        result
        == """John Doe <john@doe.org> John Doe <John@Doe.org>
John Doe <john@doe.org> John Doe <John@doe.org>
John Doe <john@doe.org> john doe <john@doe.org>
Max Example <hi@test.com> Max Exa <max@test.co.uk>
"""
    )


def test_create_mailmap_on_email_address():
    authors = (
        "John Doe <john@doe.org>",
        "john doe <john@doe.org>",
    )

    expected_result = (("John Doe <john@doe.org>", "john doe <john@doe.org>"),)
    expected_result_df = pd.DataFrame(expected_result, columns=("author", "author_alias"))

    result = compute_mailmap(authors)
    result_df = pd.DataFrame(result, columns=("author", "author_alias"))
    assert result_df.equals(expected_result_df)


def test_create_mailmap():
    authors = [
        "John Doe <John@Doe.org>",
        "John Doe <John@doe.org>",
        "John Doe <john@doe.org>",
        "Max Exa <max@test.co.uk>",
        "Max Example <hi@test.com>",
        "john doe <john@doe.org>",
    ]

    expected_result = [
        ("John Doe <John@Doe.org>", "John Doe <John@doe.org>"),
        ("John Doe <John@Doe.org>", "John Doe <john@doe.org>"),
        ("John Doe <John@Doe.org>", "john doe <john@doe.org>"),
        ("Max Exa <max@test.co.uk>", ""),
        ("Max Example <hi@test.com>", ""),
    ]
    expected_result_df = pd.DataFrame(expected_result, columns=("author", "author_alias"))

    result = compute_mailmap(authors)
    result_df = pd.DataFrame(result, columns=("author", "author_alias"))
    assert result_df.equals(expected_result_df)


@pytest.mark.skip(reason="Not implemented yet...")
def test_create_mailmap_no_similarity():
    authors = [
        "John Doe <John@Doe.org>",
        "John Doe <John@doe.org>",
        "John Doe <john@doe.org>",
        "Max Exa <max@test.co.uk>",
        "Max Example <hi@test.com>",
        "john doe <john@doe.org>",
    ]

    expected_result = [
        ("John Doe <John@Doe.org>", "John Doe <John@doe.org>"),
        ("John Doe <John@Doe.org>", "John Doe <john@doe.org>"),
        ("John Doe <John@Doe.org>", "john doe <john@doe.org>"),
        ("Max Exa <max@test.co.uk>", "Max Example <hi@test.com>"),
    ]
    result = compute_mailmap(authors)
    assert result == expected_result


def test_real_case():
    with tempfile.TemporaryDirectory() as tmpdirname:
        git_repo_url = "https://github.com/psf/requests"
        cmd = f"git clone {git_repo_url} {tmpdirname}"
        result = subprocess.run(cmd, shell=True, capture_output=True, text=True)

        cmd = f"git -C {tmpdirname} checkout 5c1f72e80a7d7ac129631ea5b0c34c7876bc6ed7"
        result = subprocess.run(cmd, shell=True, capture_output=True, text=True)

        mailmap_str = create_mailmap(tmpdirname)

        expected_results = [
            "Nate Prewitt <Nate.Prewitt@gmail.com> Nate Prewitt <nate.prewitt@gmail.com>",
            "Nate Prewitt <Nate.Prewitt@gmail.com> Nate Prewitt <nateprewitt@users.noreply.github.com>",
            "Kenneth Reitz <_@kennethreitz.com> Kenneth Reitz <kreitz@Kenneths-MacBook-Pro.local>",
            "Kenneth Reitz <_@kennethreitz.com> Kenneth Reitz <me@kennethreitz.com>",
            "Kenneth Reitz <_@kennethreitz.com> Kenneth Reitz <me@kennethreitz.org>",
            "Michael Komitee <komitee@deshaw.com> Michael Komitee <mkomitee@gmail.com>",
            "Nicola Soranzo <nicola.soranzo@earlham.ac.uk> Nicola Soranzo <nsoranzo@tiscali.it>",
            "Ian Cordasco <ian.cordasco@rackspace.com> Ian Cordasco <icordasc+github@coglib.com>",
            "Ian Cordasco <ian.cordasco@rackspace.com> Ian Cordasco <graffatcolmingov@gmail.com>",
            "Ian Cordasco <ian.cordasco@rackspace.com> Ian Stapleton Cordasco <graffatcolmingov@gmail.com>",
            "Ian Cordasco <ian.cordasco@rackspace.com> Ian Stapleton Cordasco <sigmavirus24@users.noreply.github.com>",
            "Ian Cordasco <ian.cordasco@rackspace.com> Ian Cordasco <sigmavirus24@users.noreply.github.com>",
        ]

        for line in expected_results:
            assert line in mailmap_str
